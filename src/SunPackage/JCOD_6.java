package SunPackage;

import java.util.Arrays;

public class JCOD_6 {

	public static void moveZerosToRight(int arr[]) {

		int count = 0;
		for (int i = 0; i < arr.length; i++) {

			if (arr[i] != 0) {
				arr[count] = arr[i];
				count++;
			}

		}

		while (count < arr.length) {
			arr[count] = 0;
			count++;
		}

		System.out.println(Arrays.toString(arr));

	}

	public static void main(String[] args) {

		int[] arr = { 1, 0, 2, 3, 0, 0, 4, 5 };
		moveZerosToRight(arr);

	}

}
