package Pattern;

public class IsPrime {

	public static void main(String[] args) {
		
		int num=3;
		boolean isPrime=true;
		
		for (int i = 2; i <num; i++) {
			
			if(num%i==0)
			{
				isPrime=false;
				break;
			}
			
		}
		
		if(isPrime)
		{
			System.out.println("Prime number");
		}else {
			System.out.println("Not Prime");
		}
		
		

	}

}
