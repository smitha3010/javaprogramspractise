package SatPractise;

import java.util.Arrays;

public class Anagram {

	public static void main(String[] args) {

		String s1 = "stops";
		String s2 = "sspot";

		if (s1.length() == s2.length()) {

			char[] ch1 = s1.toCharArray();
			char[] ch2 = s2.toCharArray();

			Arrays.sort(ch1);

			Arrays.sort(ch2);

			boolean result = Arrays.equals(ch1, ch2);

			if (result) {
				System.out.print(s1 + " and " + s2 + " are anagram");
			} else {
				System.out.print(s1 + " and " + s2 + " are not anagram");
			}

		} else {
			System.out.print(s1 + s2 + "are not anagram");
		}

	}

}
