package Pattern;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListPrac {

	public static void main(String[] args) {

		List<Integer> aList = new ArrayList<Integer>();

		aList.add(99);

		aList.add(19);

		aList.add(39);

		aList.add(69);

		aList.add(69);
		aList.add(1, 100);

		System.out.println(aList.size());
		System.out.println(aList);
		System.out.println("+++++++++++++++");

		for (int i = 0; i < aList.size(); i++) {

			System.out.println(aList.get(i));

		}

		System.out.println("++++++++++++++++++");

		for (Integer integer : aList) {

			System.out.println(integer);

		}

		aList.remove(0);

		System.out.println(aList);

		System.out.println(aList.contains(100));

		// aList.clear();
		// aList.isEmpty();

		Collections.sort(aList);
		System.out.println(aList);
		Collections.reverse(aList);

		System.out.println(aList);

	}

}
