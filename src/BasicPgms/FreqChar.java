package BasicPgms;

import java.util.HashMap;
import java.util.Map;

public class FreqChar {
	
	
	public static void charCount(String str) {
		
		
	char[] ch=str.toCharArray();
	Map<Character,Integer> countie=new HashMap<Character,Integer>();
	
	for (char c : ch) {
		
		if(countie.containsKey(c))
		{
			countie.put(c, countie.get(c)+1);
		}else
		{
			countie.put(c, 1);
		}
		
	}
	System.out.print(str+" "+countie);
		

	}

	public static void main(String[] args) {
		
		String str="Hello";
		
		charCount(str);

	}

}
