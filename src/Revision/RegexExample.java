package Revision;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexExample {

	public static void main(String[] args) {

		// String companyInfo="Amazon has more than 51000 employess across 18 states in
		// India";
		String text = "My age is 37 and my son age is 17";

		String pattern = "[\\d]+";

		Pattern compile = Pattern.compile(pattern);
		Matcher matcher = compile.matcher(text);

		int sum = 0;

		while (matcher.find()) {
			sum = sum + Integer.parseInt(matcher.group());

		}
		System.out.println(sum);

	}

}
