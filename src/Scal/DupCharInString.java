package Scal;

import java.util.HashMap;
import java.util.Set;

public class DupCharInString {

	private static void charcount(String s) {

		HashMap<Character, Integer> charCountie = new HashMap<Character, Integer>();

		char[] ch = s.toCharArray();

		for (char c : ch) {

			if (charCountie.containsKey(c)) {
				charCountie.put(c, charCountie.get(c) + 1);
			} else {
				charCountie.put(c, 1);
			}

		}
		Set<Character> chs = charCountie.keySet();

		for (Character character : chs) {

			if (charCountie.get(character) > 1) {
				System.out.println(character + " " + charCountie.get(character));
			}

		}

	}

	public static void main(String[] args) {
		charcount("JavaJ2EEz");

	}

}
