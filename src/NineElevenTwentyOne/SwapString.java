package NineElevenTwentyOne;

public class SwapString {

	public static void main(String[] args) {
		
		
		String a = "love";
		String b="You";
		System.out.println("Before swap: "+a+" "+b);
		
		a=a+b;
		
		b=a.substring(0,a.length()-b.length());
		System.out.println(b);
		
		a=a.substring(b.length());
		
		System.out.println(a);
		System.out.println(" After Swap"+ a+" "+b);
		
		
		
		
	}

}
