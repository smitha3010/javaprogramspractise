package Pattern;

public class DerivedClass extends BaseClass {

	public void baseClass() {

		super.baseClass();

		System.out.println("Child Class");

	}

	public static void main(String[] args) {

		DerivedClass cla = new DerivedClass();
	

		cla.baseClass();

	}

}
