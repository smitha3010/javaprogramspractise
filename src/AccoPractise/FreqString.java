package AccoPractise;

import java.util.HashMap;
import java.util.Map;

public class FreqString {

	public static void charCount(String str) {

		Map<Character, Integer> countie = new HashMap<Character, Integer>();

		char[] ch = str.toCharArray();

		for (char c : ch) {

			if (countie.containsKey(c)) {
				countie.put(c, countie.get(c) + 1);
			} else {
				countie.put(c, 1);
			}

		}

		System.out.print(str + " " + countie);
	}

	public static void main(String[] args) {
		String str = "Hello";

		charCount(str);
	}

}
