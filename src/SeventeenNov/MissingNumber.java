package SeventeenNov;

public class MissingNumber {

	public static void main(String[] args) {

		int[] arr = { 1, 3, 4, 6, 8 };

		int len = arr[arr.length - 1];

		int count = 0;

		for (int i = 0; i < len; i++) {

			if (arr[count] == i) {
				count++;
			} else {
				System.out.println(i + " ");
			}

		}
	}

}
