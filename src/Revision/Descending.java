package Revision;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Descending {

	public static void main(String[] args) {
		
		List<String> li= new ArrayList<String>();
		li.add("Apple");
		li.add("ball");
		li.add("cat");
		li.add("dog");
		
		System.out.println("Unsorted array"+li);
		
	  Collections.sort(li,Collections.reverseOrder());
	  
	  System.out.println("Sorted Array in descending order"+li);
		
	}

}
