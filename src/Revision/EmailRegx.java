package Revision;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailRegx {

	public static void main(String[] args) {
		String s="Smitha123@gmail.com";
		
		String patt = "[A-za-z0-9]+[@][a-z]+.[a-z]{2,3}";
		
		Pattern compile = Pattern.compile(patt);
		
		Matcher matcher = compile.matcher(s);
		
		System.out.println(matcher.matches());

	}

}
