package TwentyTenOct;

public class ReverseRightAnglePascal {

	public static void reversePascal(int num) {

		for (int i = num - 1; i >= 0; i--) {
			for (int j = 0; j <= i; j++) {
				System.out.print("*");

			}
			System.out.println();

		}

	}

	public static void main(String[] args) {

		int num = 10;
		reversePascal(num);

	}

}
