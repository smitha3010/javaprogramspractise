package Scal;

import java.util.HashMap;

public class DescendingOrder {
	
	
	private static void charCount(String s) {
	
		HashMap<Character,Integer> countChar=new HashMap<Character,Integer>();
		
		char[] ch=s.toCharArray();
		
		for (char c : ch) {
			
			if(countChar.containsValue(c))
			{
				countChar.put(c,countChar.get(c) +1);
			}else 
			{
				countChar.put(c, 1);
			}
			
		}
		System.out.println(s+":"+countChar);
		
		

	}

	public static void main(String[] args) {
		
		
		String s="Hello";
		charCount(s);

	}

}
