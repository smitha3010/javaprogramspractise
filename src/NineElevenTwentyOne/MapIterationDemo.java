package NineElevenTwentyOne;

import java.util.HashMap;
import java.util.Map;

public class MapIterationDemo {

	public static void main(String[] args) {
	
		
		Map<String,String> g= new HashMap<String,String>();
		
		g.put("A", "Smitha1");
		g.put("B", "Smitha2");
		g.put("C", "Smitha3");
		g.put("D", "Smitha4");
		
		for (Map.Entry<String, String> entry: g.entrySet()) {
			
			System.out.println(entry.getKey() +" "+entry.getValue());
			
		}
		
		
		

	}

}
