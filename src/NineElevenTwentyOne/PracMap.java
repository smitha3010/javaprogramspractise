package NineElevenTwentyOne;

import java.util.HashMap;
import java.util.Map;

public class PracMap {

	public static void main(String[] args) {

		HashMap<Integer, String> val = new HashMap<Integer, String>();

		val.put(1, "Apple");

		val.put(2, "ball");
		val.put(3, "cat");
		val.put(4, "cat");

		System.out.println(val);

		System.out.println(val.get(1));// Apple
		System.out.println(val.containsKey(1));
		System.out.println(val.containsValue("kothi"));
		
		
		System.out.println(val.keySet());
		
		for (Integer s : val.keySet()) {
			
			System.out.println(s+" "+val.get(s));
			
		}
		
		val.values();
		
		for (String string : val.values()) {
			
			System.out.println(string);
			
		}

		for (Map.Entry<Integer, String> entry : val.entrySet()) {

			System.out.println(entry.getKey()+" "+entry.getValue());

		}

	}

}
