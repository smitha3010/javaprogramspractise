package OneElevenNov;

public class RevString {
	
	
	
	public static void main(String[] args) {
		
		String str="Happy KannadaRajyotsava";
		
		//method 1
		
		for (int i=str.length()-1;i>=0;i--) {
			
			System.out.print(str.charAt(i));
			
		}
		System.out.println();
		
		//method 2
		
		
		char[] ch=str.toCharArray();
		
		for (int i = ch.length-1; i>=0;i--) {
			
			System.out.print(ch[i]);
		}
		
		System.out.println();
		StringBuilder sh=new StringBuilder(str);
		
		sh.reverse();
		
		System.out.println(sh);
		
		

	}

}
