package Pattern;

public class MissingNumber {

	public static void main(String[] args) {
		
		int arr[]= {1,2,3,5,7,8};
		
		int len=arr[arr.length-1];
		
		int count=0;
		
		for (int i = 0; i < len; i++) {
			if(arr[count]==i)
			{
				count++;
			}else
			{
				System.out.print(i+ " ");
			}
			
		}
		

	}

}
