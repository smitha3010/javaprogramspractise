package Scal;

import java.util.HashMap;

public class Freq {

	public static void charCount(String s) {

		HashMap<Character, Integer> ch1 = new HashMap<Character, Integer>();

		char[] ch = s.toCharArray();

		for (char c : ch) {

			if (ch1.containsKey(c)) {
				ch1.put(c, ch1.get(c) + 1);
			} else {
				ch1.put(c, 1);
			}

		}
		System.out.print(s + " " + ch1);

	}

	public static void main(String[] args) {

		String s = "Hello";

		charCount(s);

	}

}
