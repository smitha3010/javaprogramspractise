package SunPackage;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class JCOD_4 {

	public static void charCount(String s) {

		Map<Character, Integer> mie = new HashMap<Character, Integer>();

		char[] ch = s.toCharArray();
		
		
		for (char c : ch) {
			
			if(mie.containsKey(c))
			{
				mie.put(c,mie.get(c)+1);
				
			}else
			{
				mie.put(c, 1);
			}
			
		}
		
		//System.out.print(s+" "+mie);
		
		
		Set<Character> se= mie.keySet();
		
		for (Character character : se) {
			
			if(mie.get(character)>1)
			{
				System.out.println(character+":"+mie.get(character));
			}
			
			
			
		}

	}

	public static void main(String[] args) {

		String s = "Better Butter";
		charCount(s.replaceAll("\\s", ""));
		
		
		
		

	}

}
