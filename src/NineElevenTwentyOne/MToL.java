//package NineElevenTwentyOne;
//
//
//		// Java Program to Convert Map to List
//
//		// Importing required classes
//		import java.util.*;
//
//		class GFG {
//
//			// Method 1
//			public static void main(String[] args)
//			{
//
//				// Creating HashMap
//				HashMap<String, Integer> hs = new HashMap<>();
//
//				// Adding elements to hashMap
//				hs.put("Geeks", 1);
//				hs.put("for", 2);
//				hs.put("Geeks", 3);
//				hs.put("Computer", 4);
//				hs.put("Science", 5);
//				hs.put("Portal", 6);
//
//				// Calling method
//				MapValuesToList obj = new MapValuesToList(hs);
//
//				// Storing into List
//				List<Integer> mapvaltolist = obj.mapvaltolist(hs);
//
//				// Printing via for loops
//				for (Integer integerList : mapvaltolist) {
//
//					// Printing our ArrayList
//					System.out.println(integerList);
//				}
//			}
//
//			// Method 2
//			// To convert Map to List
//			public List<String> mapvaltolist(Map<String, Integer> x)
//			{
//
//				// Using Collection
//				Collection<Integer> val = map.values();
//
//				// Creating an ArrayList
//				ArrayList<Integer> al = new ArrayList<>(values);
//
//				return al;
//			}
//		}
//
//	}
//
//}
