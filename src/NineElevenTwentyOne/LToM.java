package NineElevenTwentyOne;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LToM {
	
	
	private Integer id;
	
	private String name;
	
	public LToM(Integer id,String name)
	{
		this.id=id;
		this.name=name;
	}
	
	
	public String getName()
	{
		return name;
	}
	
	public Integer getId()
	{
		return id;
	}
	

	public static void main(String[] args) {
		
		
		
		List<LToM> lt=new ArrayList<LToM>();
		
		lt.add(new LToM(1,"Smitha") );
		lt.add(new LToM(2,"d") );
		lt.add(new LToM(3,"dbhd") );
		lt.add(new LToM(4,"dkehgdueg") );
		
		Map<Integer,String> mp=new HashMap<Integer,String>();
		
		for (LToM lToM : lt) {
			
			mp.put(lToM.getId(),lToM.getName() );
			
		}
		
		System.out.println("Map"+mp);
		
		
	}

}
