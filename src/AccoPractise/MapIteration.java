package AccoPractise;

import java.util.HashMap;
import java.util.Map;

public class MapIteration {

	public static void main(String[] args) {
		
		
		Map<Integer,String> ch=new HashMap<Integer,String>();
		
		ch.put(1, "Apple");
		ch.put(2, "Ball");
		ch.put(3, "Cat");
		ch.put(4, "Dog");
		
		
		for (Map.Entry<Integer, String> c : ch.entrySet()) {
			
			System.out.println(c.getKey()+" "+c.getValue());
			
			
			
		}
		
		

	}

}
