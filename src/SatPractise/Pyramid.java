package SatPractise;

public class Pyramid {

	public static void main(String[] args) {

		int i = 1;

		while (i <= 5) {
			int j = 1;

			while (j <= 5) {

				if ((i + j) <= 5) {
					System.out.print(" ");

				} else {
					System.out.print(i);
					System.out.print(" ");
				}
				j++;
			}

			i++;
			System.out.println();
		}

	}

}
