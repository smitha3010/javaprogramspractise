package Pattern;

public class NumberPalindrome {

	public static void main(String[] args) {
		
		int num=151;
		
		int temp=num;
		
		int res=0;
		int rem;
		
		while(num>0)
		{
			rem=num%10;
			res=res*10+rem;
			num=num/10;
		}
		System.out.println(res);
		if(res==temp)
		{
			System.out.println("its a palindrome");
		}else
		{
			System.out.println("Not a palindrome");
		}
	}

}
