package SeventeenNov;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DescendingOrder {

	public static void main(String[] args) {
		
		List<Integer> li=new ArrayList<Integer>();
		
		li.add(1);
		li.add(4);
		li.add(8);
		li.add(2);
		li.add(6);
		
		
		System.out.println("Before"+li);
		
		Collections.sort(li,Collections.reverseOrder());
		
		System.out.println("Descending Order"+li);

	}

}
