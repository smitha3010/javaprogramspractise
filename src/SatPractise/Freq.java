package SatPractise;

import java.util.HashMap;
import java.util.Map;

public class Freq {

	public static void charCount(String s) {

		Map<Character, Integer> charCountie = new HashMap<Character, Integer>();

		char[] ch = s.toCharArray();

		for (char c : ch) {

			if (charCountie.containsKey(c)) {
				charCountie.put(c, charCountie.get(c) + 1);
			} else

			{
				charCountie.put(c, 1);
			}

		}

		System.out.print(s + " " + charCountie);

	}

	public static void main(String[] args) {

		String s = "Hello";

		charCount(s);

	}

}
