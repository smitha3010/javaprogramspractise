package Regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AmazonRegex {

	public static void main(String[] args) {

		String str = "Amazon is having 51000 employees across 18 states";
		
		String pattern = "[\\d]+";
		Pattern compile=Pattern.compile(pattern);
		
		Matcher matcher = compile.matcher(str);
		
		int sum=0;
		
		while(matcher.find())
		{
			sum=sum+Integer.parseInt(matcher.group());
		}
		System.out.println(sum);


	}

}
