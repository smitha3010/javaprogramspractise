package SeventeenNov;

public class StringPal {

	public static void main(String[] args) {

		String pal = "madam";

		String res = "";

		for (int i = pal.length() - 1; i >= 0; i--) {

			char ch = pal.charAt(i);

			res = res + ch;

		}

		if (res.equals(pal)) {
			System.out.println("palindrome");
		} else {
			System.out.println("Not a palindrome");
		}

	}

}
